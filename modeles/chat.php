
<?php
include("../modeles/connexion_bdd.php");
function setMessage($pseudo, $message)
{
    $bdd=connexion();
    $reponse=$bdd->prepare('INSERT INTO message (pseudo_utilisateur,message) VALUES (?,?)');
    $reponse->execute(array($pseudo, $message));
    
    
}
function getMessages($numMessageDepart, $nbMessagesRetour)
{
    
    $bdd=connexion();
    $requete='SELECT * FROM message ORDER BY ID DESC LIMIT '.$numMessageDepart.','.$nbMessagesRetour;
    $response=$bdd->query($requete);
    $i=0;
    while ($donnee=$response->fetch())
    {
        $messages[$i]['date']=$donnee['date'];
        $messages[$i]['pseudo']=$donnee['pseudo_utilisateur'];
        $messages[$i]['message']=$donnee['message'];
        $i++;
    }
    $response->closeCursor();
    return $messages;
}
?>